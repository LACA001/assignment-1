# Homework 01, 2018.01.29:  7/10 #

### Kudos ###

* Good use of a StackLayout to organize multiple elements. To prevent your content from overlapping the status bar on iOS, use the ["UseSafeArea" api](https://bitbucket.org/snippets/jbachelorcsusm/pey7Eb/set-app-to-respect-ios-safe-area).
* Good exploration of Label properties you can use to customize your UI.
* Very cool that you were exploring customizing a label with the "MultiLineLabel" class, even if you didn't wind up using it. Be aware that [labels are already capable of multi-line text](https://developer.xamarin.com/api/type/Xamarin.Forms.Label/).

### Opportunities for improvement ###

* Cloning a repository is an important skill, but for this class, please create your solutions from scratch (unless otherwise instructed).
* Please remember to turn in your assignments on time. I will always provide feedback, but late assignments will not receive credit in the future. I'm just taking off 3 points this time.