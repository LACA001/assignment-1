﻿using System.Diagnostics;
using Xamarin.Forms;

namespace HelloUniverse
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new HelloUniversePage();
        }

        public class MultiLineLabel : Label
        {
            private static int _defaultLineSetting = -1;

            public static readonly BindableProperty LinesProperty = BindableProperty.Create(nameof(Lines), typeof(int), typeof(MultiLineLabel), _defaultLineSetting);
            public int Lines
            {
                get { return (int)GetValue(LinesProperty); }
                set { SetValue(LinesProperty, value); }
            }
        }   
         
        protected override void OnStart()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnStart)}");
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnSleep)}");
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnResume)}");
        }
    }
}
